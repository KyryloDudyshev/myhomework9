package myhomework9.main.java.com.danit.hw9;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {
        //first family
        Man john = new Man("John","Wick","03/03/1686",89);
        john.setIq(150);
        HashMap<DayOfWeek,String> scheduleJohn = new HashMap<DayOfWeek,String>();
        scheduleJohn.put(DayOfWeek.SATURDAY,"Play football");
        scheduleJohn.put(DayOfWeek.SUNDAY,"Goto the gym");
        john.setSchedule(scheduleJohn);
        Woman karla = new Woman("Karla","Bruni","03/03/1686",89);
        karla.setIq(150);
        HashMap<DayOfWeek,String> scheduleKarla = new HashMap<DayOfWeek,String>();
        scheduleKarla.put(DayOfWeek.SATURDAY,"Go shopping");
        scheduleKarla.put(DayOfWeek.SUNDAY,"Play tennis");
        karla.setSchedule(scheduleKarla);
        Family familyWick = new Family(karla, john);
        familyWick.setMother(karla);
        Dog dog = new Dog("Alfa",3,95, new HashSet<String>(Set.of("play","go for a walk")));
        dog.getSpecies().setNumberOfPaw(4);
        dog.getSpecies().setHasFur(true);
        dog.getSpecies().setCanFly(false);
        familyWick.setPet(dog);
        Woman natali = new Woman("Natali","Portman","03/03/1686",89);
        familyWick.addChild(natali);
        natali.setFamily(familyWick);
        Woman liza = new Woman("Liza","Mineli","03/03/1686",89);
        familyWick.addChild(liza);
        liza.setFamily(familyWick);

        //second family
        Man arnold = new Man("Arnold","Schwarzenegger","03/03/1686",89);
        arnold.setIq(200);
        HashMap<DayOfWeek,String> scheduleArnold = new HashMap<DayOfWeek,String>();
        scheduleArnold.put(DayOfWeek.SATURDAY,"Go to the gym");
        scheduleArnold.put(DayOfWeek.SUNDAY,"Go to the gym");
        arnold.setSchedule(scheduleArnold);
        Woman monica = new Woman("Monica","Belucci","03/03/1686",89);
        monica.setIq(150);
        HashMap<DayOfWeek,String> scheduleMonica = new HashMap<DayOfWeek,String>();
        scheduleMonica.put(DayOfWeek.SATURDAY,"Visit Jennifer");
        scheduleMonica.put(DayOfWeek.SUNDAY,"Go to the bar");
        monica.setSchedule(scheduleMonica);
        Family familyArnold = new Family(monica, arnold);
        DomesticCat cat = new DomesticCat("Olaf",4,50, new HashSet<String>(Set.of("sleep","eat")));
        cat.getSpecies().setCanFly(false);
        cat.getSpecies().setHasFur(true);
        cat.getSpecies().setNumberOfPaw(4);
        familyArnold.setPet(cat);
        Woman michel = new Woman("Michel","Schwarzeneger","03/03/1686",89);
        familyArnold.addChild(michel);
        michel.setFamily(familyArnold);
        HashSet<Pet> wikcsPets = new HashSet<>();
        wikcsPets.add(dog);
        familyWick.setPetsSet(wikcsPets);
        wikcsPets.add(new Fish("Solomon",1,20,new HashSet()));
        HashSet<Pet> arnoldPets = new HashSet<>();
        familyArnold.setPetsSet(arnoldPets);
        arnoldPets.add(cat);

        List<Family> familyList = new ArrayList<>();
        familyList.add(familyWick);
        familyList.add(familyArnold);

        RoboCat roboCat = new RoboCat("Martin",2,100,new HashSet<String>(Set.of("sleep","eat")));
        FamilyController familyController = new FamilyController(new FamilyService(new CollectionFamilyDao(familyList)));
        System.out.println("Вызов методов");
        System.out.println();
        System.out.println("displayAllFamilies");
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("getFamiliesBiggerThan 3");
        familyController.getFamiliesBiggerThan(3);
        System.out.println();
        System.out.println("getFamiliesLessThan 4");
        familyController.getFamiliesLessThan(4);
        System.out.println();
        System.out.println("countFamiliesWithMemberNumber 4");
        familyController.countFamiliesWithMemberNumber(4);
        System.out.println();
        System.out.println("createNewFamily");
        familyController.createNewFamily(new Woman("Suzi","Staer","03/03/1686",89),new Man("Eric","Liberman","03/03/1686",89));
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("bornChild");
        familyController.bornChild((Family) familyController.getAllFamilies().get(0),"Shon","Emma");
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("adoptChild");
        familyController.adoptChild((Family) familyController.getAllFamilies().get(1), new Man("Peter","Adopted","03/03/1686",89));
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("deleteAllChildrenOlderThen старше 23 лет");
        familyController.deleteAllChildrenOlderThen(2000);
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("count");
        System.out.println(familyController.count());
        System.out.println();
        System.out.println("getPets семьи с индексом 1");
        System.out.println(familyController.getPets(1));
        System.out.println();
        System.out.println("addPet семье с индексом 2");
        familyController.addPet(2,roboCat);
        System.out.println(familyController.getPets(2));
        john.describeAge();
        familyController.adoptChild(familyWick,new Man("Pet","So","02/12/2020",87));
        System.out.println(((Family) familyController.getAllFamilies().get(2)).getChildren().get(3));


















//        int count = 0;
//        while (count < 10000000) {
//            Man man = new Man();
//        }




    }



}
